"""
Author: Mthandazo Ndhlovu
Title: grammer_fuzzer.py
Description: Grammar based fuzzer
"""
from hashlib import new
import random
from .utils import MutationFuzzer
import re


class ExpansionError(Exception):
    pass

START_SYMBOL = "<start>"
RE_NONTERMINAL = re.compile(r'(<[^<>]*>)')
EXPR_GRAMMAR = {
    START_SYMBOL:["<expr>"],
    "<expr>":["<term> + <expr>","<term> - <expr>","<term>"],
    "<term>":["<factor> * <term>","<factor> / <term>","<factor>"],
    "<factor>":[
        "+<factor>",
        "-<factor>",
        "(<expr>)",
        "<integer>.<integer>",
        "<integer>"
    ],
    "<integer>":["<digit><integer>","<digit>"],
    "<digit>":["0","1","2","3","4","5","6","7","8","9"]
}

CGI_GRAMMAR = {
    START_SYMBOL:["<string>"],
    "<string>":["<letter>","<letter><string>"],
    "<letter>":["<plus>","<percent>","<other>"],
    "<plus>":["+"],
    "<percent>":["%<hexdigit><hexdigit>"],
    "<hexdigit>":["0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"],
    "<other>":["0","1","2","3","4","5","a","b","c","d","e","-","_"]
}

URL_GRAMMER = {
    START_SYMBOL:
        ["<url>"],
    "<url>":
        ["<scheme>://<authority><path><query>"],
    "<scheme>":
        ["http", "https", "ftp", "ftps"],
    "<authority>":
        ["<host>", "<host>:<port>", "<userinfo>@<host>", "<userinfo>@<host>:<port>"],
    "<host>":  # Just a few
        ["cispa.saarland", "www.google.com", "fuzzingbook.com"],
    "<port>":
        ["80", "8080", "<nat>"],
    "<nat>":
        ["<digit>", "<digit><digit>"],
    "<digit>":
        ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
    "<userinfo>":  # Just one
        ["user:password"],
    "<path>":  # Just a few
        ["", "/", "/<id>"],
    "<id>":  # Just a few
        ["abc", "def", "x<digit><digit>"],
    "<query>":
        ["", "?<params>"],
    "<params>":
        ["<param>", "<param>&<params>"],
    "<param>":  # Just a few
        ["<id>=<id>", "<id>=<nat>"],
}

TITLE_GRAMMAR = {
    "<start>": ["<title>"],
    "<title>": ["<topic>: <subtopic>"],
    "<topic>": ["Generating Software Tests", "<fuzzing-prefix>Fuzzing", "The Fuzzing Book"],
    "<fuzzing-prefix>": ["", "The Art of ", "The Joy of "],
    "<subtopic>": ["<subtopic-main>",
                   "<subtopic-prefix><subtopic-main>",
                   "<subtopic-main><subtopic-suffix>"],
    "<subtopic-main>": ["Breaking Software",
                        "Generating Software Tests",
                        "Principles, Techniques and Tools"],
    "<subtopic-prefix>": ["", "Tools and Techniques for "],
    "<subtopic-suffix>": [" for <reader-property> and <reader-property>",
                          " for <software-property> and <software-property>"],
    "<reader-property>": ["Fun", "Profit"],
    "<software-property>": ["Robustness", "Reliability", "Security"],
}

def nonterminals(expansion):
    if isinstance(expansion, tuple):
        expansion = expansion[0]
    return re.findall(RE_NONTERMINAL,expansion)

def is_nonterminal(s):
    return re.match(RE_NONTERMINAL, s)


def simple_grammar_fuzzer(grammar, start_symbol=START_SYMBOL,max_nonterminals=10,max_expansion_trials=100,log=False):
    term = START_SYMBOL
    expansion_trials = 0
    
    while len(nonterminals(term)) > 0:
        symbol_to_expand = random.choice(nonterminals(term))
        # print(symbol_to_expand)
        expansions = grammar[symbol_to_expand]
        expansion = random.choice(expansions)
        new_term = term.replace(symbol_to_expand, expansion, 1)
        # print(new_term)
        if len(nonterminals(new_term)) < max_nonterminals:
            term = new_term
            if log:
                print("%-40s" % (symbol_to_expand + " -> " + expansion), term)
            expansion_trials = 0
        else:
            expansion_trials += 1
            if expansion_trials >= max_expansion_trials:
                raise ExpansionError("Cannot expand " + repr(term))
    
    return term

# print(simple_grammar_fuzzer(grammar=CGI_GRAMMAR, max_nonterminals=5))
number_of_seeds = 10
seeds = [
    simple_grammar_fuzzer(
        grammar=URL_GRAMMER,
        max_nonterminals=10) for i in range(number_of_seeds)]
print(seeds)