"""
Author: Mthandazo Ndhlovu
File: utils.py
Description: utilities classes which are useful for the fuzzer methods 
"""
from hashlib import new
import time
from types import TracebackType
import subprocess
import random
from typing import Any, Dict, List, Set, Optional, Union, Tuple, Type
import sys
import os
try:
    from urlparse import urlparse
except ImportError:
    from urllib.parse import urlparse

def clock() -> float:
    return time.perf_counter()

class Timer:
    """
    Timer class which will help in calculating the time taken by a function to finish execution
    """
    def __init__(self) -> None:
        self.start_time = clock()
        self.end_time = None

    def __enter__(self) -> Any:
        self.start_time = clock()
        self.end_time = None
        return self

    def __exit__(self, exc_type: Type, exc_value: BaseException, tb: TracebackType) -> None:
        self.end_time = clock()

    def elapsed_time(self) -> float:
        if self.end_time is None:
            return clock() - self.start_time
        else:
            return self.end_time - self.start_time


class Runner(object):
    PASS = "PASS"
    FAIL = "FAIL"
    UNRESOLVED = "UNRESOLVED"

    def __init__(self):
        """Initialize"""
        pass

    def run(self, inp):
        """Run the runner with given input"""
        return (inp, Runner.UNRESOLVED)


class PrintRunner(Runner):
    def run(self, inp):
        """Print the given input"""
        print(inp)
        return (inp, Runner.UNRESOLVED)


class ProgramRunner(Runner):
    def __init__(self, program):
        """Initialize, program is a progra spec as passed to subprocess.run()"""
        self.program = program

    def run_process(self, inp=""):
        """Run the program with inp as input. Result result of subprocess.run()"""
        return subprocess.run(self.program,input=inp,stdout=subprocess.PIPE,stderr=subprocess.PIPE,universal_newlines=True)

    def run(self, inp=""):
        """Run the program with inp as input. Return test outcome base on result of subprocess.run()"""
        result = self.run_process(inp)
        if result.returncode == 0:
            outcome = self.PASS
        elif result.returncode < 0:
            outcome = self.FAIL
        else:
            outcome = self.UNRESOLVED
        return (result, outcome)


class Fuzzer(object):
    def __init__(self):
        pass

    def fuzz(self):
        return ""

    def run(self, runner=Runner()):
        return runner.run(self.fuzz())

    def runs(self, runner=PrintRunner(),trials=10):
        outcomes = []
        for i in range(trials):
            outcomes.append(self.run(runner))
        return outcomes

class RandomFuzzer(Fuzzer):
    def __init__(self, min_length=10, max_length=100,char_start=32, char_range=32):
        self.min_length= min_length
        self.max_length = max_length
        self.char_start = char_start
        self.char_range = char_range

    def fuzz(self):
        string_length = random.randrange(self.min_length, self.max_length + 1)
        out  = ""
        for i in range(0, string_length):
            out += chr(random.randrange(self.char_start,self.char_start + self.char_range))
        return out


class FunctionRunner(Runner):
    def __init__(self, function):
        self.function = function

    def run_function(self, inp):
        return self.function(inp)

    def run(self, inp):
        try:
            result = self.run_function(inp)
            outcome = self.PASS
        except Exception:
            result = None
            outcome = self.FAIL
        return result, outcome


class Coverage(object):
    def traceit(self, frame, event, arg):
        if self.original_trace_function is not None:
            self.original_trace_function(frame, event, arg)
        if event == "line":
            function_name = frame.f_code.co_name
            lineno = frame.f_lineno
            self._trace.append((function_name,lineno))
        return self.traceit

    def __init__(self) -> None:
        self._trace = []

    def __enter__(self):
        self.original_trace_function = sys.gettrace()
        sys.settrace(self.traceit)
        return self

    def __exit__(self, exc_type, exc_value, tb):
        sys.settrace(self.original_trace_function)

    def trace(self):
        return self._trace

    def coverage(self):
        return set(self.trace())


def http_program(url):
    supported_schemes = ["http","https"]
    result = urlparse(url)
    if result.scheme not in supported_schemes:
        raise ValueError("Scheme must be one of " + repr(supported_schemes))
    if result.netloc == '':
        raise ValueError("Host must be non-empty")
    return True

# with Coverage() as cov:
#     http_program("http://www.facebook.com")
# print(cov.coverage())

def delete_random_character(s):
    if s == "":
        return s
    pos = random.randint(0, len(s) - 1)
    return s[:pos] + s[pos + 1:]


def insert_random_character(s):
    pos = random.randint(0, len(s))
    random_character = chr(random.randrange(32, 127))
    return s[:pos] + random_character + s[pos:]


def flip_random_character(s):
    if s == "":
        return s
    pos = random.randint(0, len(s) - 1)
    c = s[pos]
    bit = 1 << random.randint(0, 6)
    new_c = chr(ord(c) ^ bit)
    return s[:pos] + new_c + s[pos + 1:]



def mutate(s):
    mutators = [
        delete_random_character,
        insert_random_character,
        flip_random_character,
    ]
    mutator = random.choice(mutators)
    return mutator(s)


def is_valid_url(url):
    try:
        result = http_program(url)
        return True
    except ValueError:
        return False


class MutationFuzzer(Fuzzer):
    def __init__(self,seed, min_mutations=2,max_mutations=10):
        self.seed = seed
        self.min_mutations = min_mutations
        self.max_mutations = max_mutations
        self.reset()

    def reset(self):
        self.population = self.seed
        self.seed_index = 0

    def mutate(self, inp):
        return mutate(inp)

    def create_candidate(self):
        candidate = random.choice(self.population)
        trials = random.randint(self.min_mutations,self.max_mutations)
        for i in range(trials):
            candidate = self.mutate(candidate)
        return candidate

    def fuzz(self):
        if self.seed_index < len(self.seed):
            self.inp = self.seed[self.seed_index]
            self.seed_index += 1
            # self.inp = self.create_candidate()
        else:
            self.inp = self.create_candidate()
        return self.inp

# seed_input = "http://www.google.com"
# http_runner = FunctionRunner(http_program)
# print(http_runner.run(seed_input))
# mutation_fuzzer = MutationFuzzer(seed=[seed_input])
# for i in range(10):
#     print(mutation_fuzzer.fuzz())