"""
Author: Mthandazo Ndhlovu
File: main.py
Description: main simple fuzzer 
"""
#!/usr/bin/python
import os
import sys
import random
from hashlib import md5


class BasicFuzzer:
    def __init__(self, file_in, folder_out, cmd=None):
        """
        Set the directories and the OS command to run after mutating
        """
        self.folder_out = folder_out
        self.file_in = file_in
        self.cmd = cmd
        self.raw_buf = None
        self.fuzz_buf = None

    def mutate(self):
        tmp = bytearray(self.raw_buf)
        # Calculate the total number of changes to made to the buffer
        total_changes = random.randint(1, len(tmp))
        for i in range(total_changes):
            # Select a random position in the file
            pos = random.randint(0, len(tmp) - 1)
            # Select the character to replace
            char = chr(random.randint(0, 255))
            # Finally, replace the content at the selected position the
            # new randomly selected character
            tmp[pos] = random.randint(0, 255)
        self.fuzz_buf = tmp
        return str(tmp)

    def file_input_and_output(self, flag=0,filename=None):
        try:
            if flag == 0:
                with open(self.file_in, "rb") as file_buf:
                    self.raw_buf = file_buf.read()
                file_buf.close()
            elif flag == 1:
                with open(filename, "wb") as file_buf:
                    file_buf.write(self.fuzz_buf)
                file_buf.close()
        except Exception as e:
            print(e)

    def fuzz(self):
        # Create 255 mutations of the input file
        try:
            for i in range(255):
                buf = self.mutate()
                md5_hash = md5(buf.encode()).hexdigest()
                print("[+] Writing mutate file %s\n" % repr(md5_hash))
                if os.path.exists(self.folder_out) and os.path.isdir(self.folder_out):
                    pass
                else:
                    os.mkdir(self.folder_out)
                filename = os.path.join(self.folder_out, str(md5_hash))
                self.file_input_and_output(flag=1,filename=filename)
        except Exception as e:
            print("BINGO")
            print(e)

def usage():
    print("Usage: ", sys.argv[0], "<filename> <output directory>")

def main(file_in, folder_out, cmd=None):
    fuzzer = BasicFuzzer(file_in, folder_out, cmd)
    fuzzer.file_input_and_output()
    fuzzer.fuzz()

if __name__ == "__main__":
    if len(sys.argv) != 3:
        usage()
    else:
        main(sys.argv[1], sys.argv[2])